\ ******************************************************************
\ ARRAY.FS IS PART OF THE MF-RP2040 PROJECT
\ SPDX-FileCopyrightText: 2022 Christopher Howard <christopher@librehacker.com>
\ SPDX-License-Identifier: GPL-3.0-or-later
\
\ PROVIDES WORDS TO MAKE CODING ARRAYS MORE CONCISE
\
\ PUBLIC WORDS:
\   C| [|
\ ******************************************************************

M_ARRAY
MARKER M_ARRAY

\ ******************************************************************
\ FOR EACH TOKEN AFTER "[|", EXECUTES THAT TOKEN, AND CALLS ",". THE
\ PROCEDURE ENDS WHEN A "|]" TOKEN IS ENCOUNTERED. EXAMPLES:
\   CREATE FOO [| 1 2 3 4 |]
\   $C CONSTANT X
\   CREATE MYARRAY [| $A %1011 X |]
\ CAVEATS:
\   - YOU CANNOT NEST ARRAYS
\   - AN ARRAY MUST BE CONFINED TO ONE LINE
\ HOWEVER, YOU CAN HAVE ARRAYS ONE AFTER ANOTHER, LIKE SO:
\   CREATE BIGARRAY
\   [| 1 2 3 |]
\   [| 4 5 6 |]
\   [| 7 8 9 |]
\ THIS IS EQUIVALENT TO [| 1 2 3 4 5 6 7 8 9 |]
\ ******************************************************************
: [| BEGIN TOKEN 2DUP S" |]" COMPARE
    IF 2DROP TRUE ELSE EVALUATE , FALSE THEN UNTIL ;

\ ******************************************************************

\ ******************************************************************
\ SAME AS [|, BUT USING C, INSTEAD
\ ******************************************************************
: C| BEGIN TOKEN 2DUP S" |C" COMPARE
    IF 2DROP TRUE ELSE EVALUATE C, FALSE THEN UNTIL ;
