# Copyright and Licensing

[![REUSE status](https://api.reuse.software/badge/codeberg.org/infrared/PROG-AT28C64B)](https://api.reuse.software/info/codeberg.org/infrared/PROG-AT28C64B)

README.md is part of the PROG-AT28C64B project

Released as free software by permission of Tatonduk Outfitters Limited

SPDX-FileCopyrightText: 2022 Christopher Howard <christopher@librehacker.com>

SPDX-FileCopyrightText: 2022 Tatonduk Outfitters Limited

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.

# Description

This code can be used, with appropriate connections, to allow a
Raspberry Pi Pico microcontroller, running Mecrisp Stellaris Forth,
to read the data from any 2764 compatible EPROM chip, or write data
to an AT28C64B chip, and therefore to copy data from one to the other.

Currently there is no code for uploading data through the serial
connection, and therefore you must copy data from an existing chip to
program a new chip. To change this, someone would just need to write
code that somehow uploads text or binary data into the `EPROM_DATA`
buffer.

# Connections

Please see the PDF file in the `schematics` directory.

# Compiling

You will need first to install Mecrisp Stellaris 2.6.3 on your
Raspberri Pi Pico (RP2040 chip). The code uses some of the extra "tools"
words, so it is easiest just to install the
"mecrisp-stellaris-pico-with-tools.uf2" file from the rp2040-ra
directory.

Then login to the microcontroller and compile the modules
in this order:

MARKER.FS
DICTIONARY.FS
REGISTERS.FS
INIT.FS
OPERATIONS.FS
ARRAY.FS
CRC32.FS
SHELL.FS

Personally, I use this command to login on my system:

`picocom --send-cmd "ascii-xfr -l 10 -s" -b 115200 --imap "lfcrlf" /dev/ttyUSB0`

Then I run the command COMPILETOFLASH.

And then I use picocom's CTRL-A CTRL-S shortcut to upload the code from
each file.

Then SAVE will make the code persistent across reboots.

# Procedures

Please see the documentation at the top of the `firmware/SHELL.FS` file.
